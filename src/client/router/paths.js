import DashboardView from '@/views/Dashboard'

export default [
  {
    path: '/dashboard',
    alias: '',
    component: DashboardView,
    name: 'Dashboard',
    view: 'Dashboard',
    meta: { description: 'Overview of enviroment' }
  }
]
