# Air Quality Check (in progress...)

## General info
Air Quality Check is a Vue.js SPA application that allows you to monitor air quality across Poland. It uses Firebase for storing data, JSON API developed by Chief Inspectorate for Environmental Protection and Vue.js libraries like Vue2Leaflet and vue-chartjs.



## Project setup
In relation with headers setup in API to be able to communicate with it you have to download and run my node.js API which you can find [here](https://bitbucket.org/mStolvrczyk/air-quality-check-api/src/master/).
After that: 
```
$ npm install
$ npm run serve
```
### Build project
```
npm run build
```

### Run tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```


